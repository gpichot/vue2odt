import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.Stack;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class VueDataModel {
	/**
	 * Document VUE
	 */
	protected Document document;
	/**
	 * Data
	 */
	protected Document data;
	/**
	 * Pile
	 */
	protected Stack<Element> stack;
	
	/**
	 * Factory
	 */
	protected DocumentBuilder builder;
	
	public VueDataModel() {
		
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
	    builder = docFactory.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
    }
		
	}
	
	public void generateData(String filename) {

		try {
			
			String cleanedFile = cleanXMLFile(filename);
			//parse using builder to get DOM representation of the XML file
			document = builder.parse(new InputSource(new StringReader(cleanedFile)));
			
			
		} catch(Exception e) {
			// TODO
			e.printStackTrace();
		}
		

			
	    try {
	      data = builder.parse(
	          new InputSource(
	          		new StringReader("<vue></vue>")
	          )
	      );
      } catch (SAXException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
      } catch (IOException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
      }

		
		stack = new Stack<Element>();
		stack.add(data.getDocumentElement());
		
		build();
	
	}
	
	public String cleanXMLFile(String filename) throws IOException {
		
		String name_cleanedFile = filename + "_temp";
		FileReader data = new FileReader(filename);
		FileWriter out 	= new FileWriter(name_cleanedFile, true);
		
		if(!data.ready())
			System.out.print("Fatal Error");
		
		BufferedReader buffer = new BufferedReader(data);
		
		String line;
		String str = "";
		Boolean xmlDeclarationFound = false;
		while ( (line = buffer.readLine() ) != null ) {
			
			if(!xmlDeclarationFound && line.startsWith("<?xml")) {
				xmlDeclarationFound = true;
			}
			
			if(xmlDeclarationFound)
				str += line + "\n";
			
		}
		
		buffer.close();
		
		return str;
		
	}
	
	public Document getData() {
		
		return this.data;
		
	}
	
	public void build() {
		
		build(document.getFirstChild());
		
	}

	private void build(Node node) {
		
		String tagName = node.getNodeName();
		Boolean needPop = false;
		
		if(tagName == "title") {
			appendTitle(node);
			return;
		} else if (tagName == "child") {
			needPop = appendChild(node);
		}
		
		if(node.hasChildNodes()) {
			
			NodeList children = node.getChildNodes();
			for(int i = 0; i < children.getLength(); i++) {
				
				build(children.item(i));
				
			}
			
		}
		
		if(needPop) {
			stack.pop();
		}
		
	}
	
	private Boolean appendChild(Node current) {
		
	  Node stackElement = stack.lastElement();
	  Document doc = stackElement.getOwnerDocument();
	  
	  Node label = current.getAttributes().getNamedItem("label");
	  Node arrow = current.getAttributes().getNamedItem("arrowState");
	  if(label != null) {
	  
		  Element child = doc.createElement("child");
		  Node title = doc.createElement("title");
		  
		  title.appendChild(
		  	doc.createTextNode(
		  		label.getTextContent().replaceAll("\n", "")
		  	)
		  );
		  
		  
		  child.setAttribute(
	  		"id", 
	  		current.getAttributes().getNamedItem("ID").getNodeValue()
		  );
		  child.appendChild(title);
		  
		 
		  
		  stackElement.appendChild(child);
		  
		  stack.push(child);
		  
		  return true;
	  } else if( arrow != null) {
	  	
	  	String id1 = ((Element) current).getElementsByTagName("ID1").item(0).getTextContent();
	  	String id2 = ((Element) current).getElementsByTagName("ID2").item(0).getTextContent();
	  	
	  	XPathFactory factory = XPathFactory.newInstance();
	  	XPath xpath = factory.newXPath();
	  	XPathExpression expr1, expr2;
	  	
	  
      try {
	      expr1 = xpath.compile("//child[@id='" + id1 + "']");
	      expr2 = xpath.compile("//child[@id='" + id2 + "']");
	      
	     	NodeList result1, result2;
	      try {
		      result1 = (NodeList) expr1.evaluate(data, XPathConstants.NODESET);
		      result2 = (NodeList) expr2.evaluate(data, XPathConstants.NODESET);
		      
			  	Node parent = ((NodeList) result1).item(0);
			  	Node child  = ((NodeList) result2).item(0);
			  	
			  	parent.appendChild(child);
			  
	      } catch (XPathExpressionException e) {
		      // TODO Auto-generated catch block
		      e.printStackTrace();
	      }
		  	

		  	
      } catch (XPathExpressionException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
      }

      return false;
	  	
	  } else return false;
	  
  }

	private void appendTitle(Node current) {
		
		Node stackElement = stack.lastElement();
		
		Document doc = stackElement.getOwnerDocument();

		Node title = doc.createElement("title");
		title.appendChild(doc.createTextNode(current.getTextContent()));
	
		stackElement.appendChild(title);
		
	}
	
	
}

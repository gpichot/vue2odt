import java.io.File;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;


public class FileDialog extends JDialog {
	protected JFileChooser chooser = new JFileChooser();
	
	public FileDialog(String name, String ext, String description) {
		
		setTitle(name);
		setVisible(false);

		add(chooser);
		chooser.removeChoosableFileFilter(chooser.getAcceptAllFileFilter());
		chooser.setFileFilter(new CustomFileFilter(ext, description));
    
		
		
	}

	public JFileChooser getChooser() {
		return chooser;
	}
	
	public Boolean existsFile() {
		
		return existsFile(chooser.getSelectedFile().getAbsolutePath());
		
	}
	public Boolean existsFile(String filename) {
		
		File file = new File(filename);
			
		return file.exists();

	}
	
	public void show_FileDoesNotExist() {
		show_FileDoesNotExist(chooser.getSelectedFile().getName());
	}
	
	public void show_FileDoesNotExist(String filename) {
		JOptionPane.showMessageDialog(
			null, 
			"Le fichier " + filename + " n'existe pas",
			"Erreur", 
			JOptionPane.ERROR_MESSAGE
		);
	}
	
	class CustomFileFilter extends FileFilter {
		protected String description;
		protected String ext;
		
		public CustomFileFilter(String ext, String description) {
			this.description = description;
			this.ext = ext;
		}
		
		public boolean accept(File f){
			
			if(f.isDirectory()) 
				return true;
			else if(f.getName().endsWith(ext)) 
				return true;
			else 
				return false;
		}
		public String getDescription(){
			return description;
		}
	}
}

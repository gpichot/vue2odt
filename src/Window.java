import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.w3c.dom.Document;

import net.sf.jooreports.templates.DocumentTemplate;
import net.sf.jooreports.templates.DocumentTemplateException;
import net.sf.jooreports.templates.DocumentTemplateFactory;


public class Window extends JFrame{
	
	
  
  
  
	public Window() {

	
		setTitle("Vue2odt");
		setSize(350, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(3, 1));
		
		
	  // Vue
    JPanel 				panVue 			= new JPanel();
    JButton		vueButton				= new JButton("Parcourir...");
    /*FileDialog 	  vueChooser 	= new FileDialog("Choix de la vue");*/
    JTextField	 	vueField		= new JTextField();
    
    panVue.setBorder(BorderFactory.createTitledBorder("Fichier Vue"));
    
    vueField.setPreferredSize(new Dimension(160, 20));
    vueField.setEnabled(false);
    
    panVue.add(vueField);
    panVue.add(vueButton);
    
    getContentPane().add(panVue);
    
    
    // Template
    JPanel 				panTemplate  		= new JPanel();
    JButton				templateButton	= new JButton("Parcourir...");
    JTextField		templateField 	= new JTextField();
    
    panTemplate.setBorder(BorderFactory.createTitledBorder("Template"));
    
    templateField.setPreferredSize(new Dimension(160, 20));
    templateField.setEnabled(false);
    
    panTemplate.add(templateField);
    panTemplate.add(templateButton);
    
    getContentPane().add(panTemplate);
    
    
    // Output
    JPanel 				panOut			= new JPanel();
    JButton				outButton		= new JButton("Enregistrer sous...");

    panOut.setBorder(BorderFactory.createTitledBorder("Fichier de sortie"));
  
    panOut.add(outButton);
    
    getContentPane().add(panOut);
    
    
    // Events
    FileButtonListener vueListener = new FileButtonListener(
    	"Choix du fichier .vue", ".vue", "Fichier VUE", vueField
    );
    FileButtonListener templateListener = new FileButtonListener(
    	"Choix du fichier .odt template", ".odt", "Fichier ODT", templateField
    );
    vueButton.addActionListener(vueListener);
    templateButton.addActionListener(templateListener);
    outButton.addActionListener(
    	new FileSaveButtonListener(
    		"Choix du fichier de sortie",
    		".odt",
    		"Fichier ODT",
    		vueListener.getChooser(), 
    		templateListener.getChooser()
    	)
    );
    
    // Display
  	setVisible(true);
	}
	
	class FileButtonListener extends FileDialog implements ActionListener {
		protected JTextField field;
		
		public FileButtonListener(String title, String ext, String description, JTextField f) {
			super(title, ext, description);
			
			field = f;
		
		}
		
		public void actionPerformed(ActionEvent event) {
			
			if(chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				
				if(!existsFile()) {
					show_FileDoesNotExist();
				} else {
					field.setText(chooser.getSelectedFile().getAbsolutePath());
				}

				 	
	    }
			
		}

	}
	class FileSaveButtonListener extends FileDialog implements ActionListener {
		
		protected JFileChooser vueChooser;
		protected JFileChooser templateChooser;
		protected VueDataModel vueModel = new VueDataModel();
		
		protected DocumentTemplateFactory documentTemplateFactory 
			= new DocumentTemplateFactory();
		
		
		public FileSaveButtonListener(String title, String ext, String description,
				JFileChooser vue, JFileChooser template) {
			
			super(title, ext, description);
	    
			vueChooser = vue;
			templateChooser = template;
			
			chooser.setApproveButtonText("Enregistrer");
	    chooser.setApproveButtonToolTipText(
	    	"Enregistre le fichier généré sous le nom choisi"
	    );
	    
		}
		
		public void actionPerformed(ActionEvent event) {
			
			if(chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				
				if(existsFile()) {
					int option = JOptionPane.showConfirmDialog(
						null, "Voulez-vous le remplacer ?", 
						"Le fichier " + chooser.getSelectedFile() + " existe déjà", 
						JOptionPane.YES_NO_OPTION, 
						JOptionPane.QUESTION_MESSAGE
					);
								
					if(option == JOptionPane.OK_OPTION){	
						saveFile();
					}
				} else saveFile();
	    
	    }
			
		}
		
		public void saveFile() {
		
			String templatePath = templateChooser.getSelectedFile().getAbsolutePath();
			String vuePath 			= vueChooser.getSelectedFile().getAbsolutePath();
			
			// Check
			if(!existsFile(vuePath)) {
				show_FileDoesNotExist(vueChooser.getSelectedFile().getName());
				return;
			}
			if(!existsFile(templatePath)) {
				show_FileDoesNotExist(templateChooser.getSelectedFile().getName());
			}
			
			 
			DocumentTemplate template;
      try {
	      template = documentTemplateFactory.getTemplate(
	      		new File(templatePath)
	      );
	      
	      Map<String, Document> data = new HashMap<String, Document>();
				
				vueModel.generateData(vuePath);
				data.put("doc", vueModel.getData());
				
				File f = new File(chooser.getSelectedFile().getAbsolutePath());
				if(!f.exists()) {
					try {
		        f.createNewFile();
	        } catch (IOException e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
	        }
				}
				
				try {
		      template.createDocument(data, new FileOutputStream(f));
		      
		      JOptionPane.showMessageDialog(
		      	null,
		      	"Fichier généré avec succès !",
		      	"Fichier généré", 
		      	JOptionPane.INFORMATION_MESSAGE
		      );
		      
	      } catch (FileNotFoundException e) {
		      // TODO Auto-generated catch block
		      e.printStackTrace();
	      } catch (IOException e) {
		      // TODO Auto-generated catch block
		      e.printStackTrace();
	      } catch (DocumentTemplateException e) {
		      // TODO Auto-generated catch block
		      e.printStackTrace();
	      }
	      
      } catch (IOException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
      }
			
			
			
			
		}
	}
}
